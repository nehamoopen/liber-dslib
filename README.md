# LIBER Data Science in Libraries Working Group 

Hello! This repository has been transferred to the LIBER Europe GitHub organization: [https://github.com/libereurope/liber-dslib](https://github.com/libereurope/liber-dslib)