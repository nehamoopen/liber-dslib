# LIBER Data Science in Libraries Working Group 

The [LIBER Data Science in Libraries (DSLib) working group](https://libereurope.eu/working-group/liber-data-science-in-libraries-working-group/) explores and promotes library engagement in applying data science and analytical methods in libraries, taking into account all kinds of processes and workflows around library collections and metadata as well as digital infrastructures and service areas.  

This is our GitLab repository which we'll be using to maintain documents and resources while the WG is active. Feel free to take a look around and open an issue/make a pull request! 

## Goals

Areas foreseen to be investigated by the working group may include but are not limited to:

- Identify and analyse key initiatives and projects across Europe and beyond;
- Explore how data science methods and tools can be tailored to library-specific environments and requirements;
- Identify good practices as well as challenges and gaps;
- Evaluate methods and strategies for skills and capacity development.

## Contact Persons

- **WG Chair:** [Birgit Schmidt](https://libereurope.eu/member/dr-birgit-schmidt/)
- **Co-chair:** Neha Moopen
- **Co-chair:** [Péter Király](https://libereurope.eu/member/peter-kiraly-phd/)

## PS

You can also check out the [HackMD](https://hackmd.io/@nehamoopen/liber-dslib) book we're using for collaborative note-taking! A [Zotero reference library](https://www.zotero.org/groups/4344603/liber_dslib) is in the works as well.
